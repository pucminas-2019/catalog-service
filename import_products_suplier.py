import os
import requests
from pymongo import MongoClient

SUPPLIER_API = os.environ.get('SUPPLIER_API') 
SUPPLIER_KEY = os.environ.get('SUPPLIER_KEY')
SUPPLIER_ID = 'a1f063f9-1570-4683-8349-18ef1def1caa'

mongodb_url = os.environ.get('MONGODB_URL') 
db = MongoClient("storage")

print(f"Connected to: {db.server_info()}")

store_db = db['pucstore']


def import_products():

    collection = store_db.products

    print('Dropping existent supplier collection')
    collection.drop()
    print('done')
    
    print("fetching data from supplier")
    response = requests.get(SUPPLIER_API, headers={'Authorization': f'Bearer {SUPPLIER_KEY}'})
    data = response.json()

    collection.insert_many(data['products'])
    print('done') 
    
    print('Adding supplier id')
    collection.update_many({}, {'$set' : {'supplierID': SUPPLIER_ID}})
    print('done')

    print('Import finished....')
    print(f"Total items: {collection.count_documents({})}")


if __name__ == "__main__":
    import_products()
