FROM python:3.6-alpine
MAINTAINER Nilo Alexandre "n1lux.comp@gmail.com"
RUN apk add --no-cache make gcc g++
COPY . /app
WORKDIR /app
RUN pip install --upgrade pip 
RUN pip install pipenv
RUN pipenv install --system --deploy --ignore-pipfile 
ENTRYPOINT ["python"]
CMD ["app.py"]
