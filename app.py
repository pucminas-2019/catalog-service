import os
import logging 
from flask import Flask, jsonify, request
from flask_cors import CORS
import requests 
from pymongo import MongoClient
from bson.json_util import dumps


file_handler = logging.FileHandler('app.log')


MONGODB_URL = os.environ.get('MONGODB_URL')
client = MongoClient(MONGODB_URL)
#client = MongoClient('localhost', 49155)
puc_store_db = client['pucstore']

app = Flask(__name__)
CORS(app)

app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)

@app.route('/api/v1/products')
def products():
    app.logger.info('Fetching products from db')
    collection = puc_store_db['products']
    products = [p for p in collection.find({}, {'_id': False})]
    app.logger.info(f'{collection.count()} products fetched from db')
    return dumps({'products' : products}) 


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
